# Import the unittests module if you want to use the asserts from the TestCase class
# to do so your classes should inherite from unittest.TestCase
import unittest

# Import selenium
from selenium import webdriver
from selenium.webdriver.firefox.options import Options

from random import randint

# Import the Page Object Models from the pages package
from tests.pages.base import BasePage, CartPage
from tests.pages.home import HomePage
from tests.pages.product import ProductPage

driver = None


def generateRandomNumber():
    return randint(0, 20)


def setUpRun():
    global driver
    global homeP, cartP, productP

    options = Options()
    options.add_argument('-headless')

    print("Create a new Firefox session")
    driver = webdriver.Firefox(options=options)

    homeP = HomePage(
        driver, "https://altom.gitlab.io/altwalker/snipcart-jekyll-ecommerce-demo/")
    cartP = CartPage(driver)
    productP = ProductPage(driver)

    print("Set implicitly wait")
    driver.implicitly_wait(15)
    print("Window size:", driver.get_window_size())
    homeP.open()


def tearDownRun():
    global driver

    print("Close the Firefox session")
    driver.quit()


class AddProductToCart(unittest.TestCase):

    def buy_product_from_homepage(self):
        print("Buy a random product from homepage")
        homeP.add_to_cart_random_product()

    def close_cart_homepage_overlay(self):
        print("Close open cart")
        if generateRandomNumber() < 10:
            raise Exception(
                "Couldn't close shopping cart because x element couldn't be located. Note: this is a randomly generated exception.")
        cartP.wait_for_cart_reload()
        cartP.click_close_cart_button()

    def go_to_product_page(self):
        print("Select random product and go to product page")
        homeP.click_random_product()

    def go_to_homepage(self):
        print("Return on homepage by clicking on Home button")
        productP.click_home_button()

    def go_to_cart(self):
        print("Go to cart")

        homeP.click_cart_button()

    def homepage(self):
        print("Check that we are on homepage")
        print("Check that primary elements are displayed: cart button, products list containing 3 items")

        homeP.wait_for_home_button()
        self.assertTrue(homeP.is_cart_button_present())
        self.assertTrue(homeP.is_products_list_present())
        self.assertTrue(homeP.count_products() == 3)

    def homepage_cart_overlay_not_empty(self):
        print("Check that the shopping cart contains at least one product")
        self.assertTrue(cartP.is_cart_open())
        cartP.wait_for_cart_reload()
        self.assertTrue(cartP.items_in_cart() > 0)

    def product_page(self):
        print("Check that we are on product page")
        self.assertTrue(productP.is_product_page())
        self.assertTrue(productP.items_in_cart() >= 0)

    def cart_open(self):
        print("Check that shopping cart is open")
        cartP.wait_for_cart_reload()
        self.assertTrue(cartP.items_in_cart() >= 0)


class CheckoutModel(unittest.TestCase):

    def setUpModel(self):
        global driver

        print("Set up for Checkout model.")
        self.driver = driver

    # edges - actions

    def go_to_billing_address(self):
        print("Click on car Next button in order to proceed to the checkout page")
        page = CartPage(self.driver)

        page.click_content_cart_next_step_button()

    def fill_billing_and_go_to_payment(self):
        print("Fill in billing and proceed to the payment")
        page = CartPage(self.driver)
        page.fill_in_billing_adress_form(name="Altwalker", city="Cluj-Napoca", email="hello@test.test",
                                         postal_code=42012, street_address1="42 Cloud Street")
        page.click_billing_addres_next_step_button()

    def fill_payment_and_go_to_confirmation(self):
        print("Fill in payment and proceed to confirmation")
        page = CartPage(self.driver)
        page.click_payment_next_step_button()

    def place_order(self):
        print("Place the order")
        page = CartPage(self.driver)
        page.click_order_confirmation_place_order_button()

    def go_to_homepage(self):
        print("Close cart in order to get back on homepage")
        page = BasePage(self.driver)
        page.click_close_cart_button()
        page.click_home_button()

    # vertices - states

    def cart_open_and_not_empty(self):
        print("Check that there are items in cart")
        page = CartPage(self.driver)
        page.wait_for_snipcart()

        self.assertTrue(page.is_cart_open())
        self.assertTrue(page.items_in_cart() > 0)
        self.assertTrue(page.is_content_cart_view())

    def billing_address(self):
        print("Check that we've reached checkout first step: billing")
        page = CartPage(self.driver)
        page.wait_for_snipcart()
        self.assertTrue(page.is_billing_cart_view())

    def payment_method(self):
        print("Check that we've reached checkout second step: payment")
        page = CartPage(self.driver)
        page.wait_for_snipcart()
        self.assertTrue(page.is_payment_cart_view())

    def order_confirmation(self):
        print("Check that we've reached checkout last step: confirmation")
        page = CartPage(self.driver)
        page.wait_for_snipcart()
        self.assertTrue(page.is_order_confirmation_cart_view())

    def order_confirmed(self):
        print("Order is confirmed")
        page = BasePage(self.driver)
        page.wait_for_snipcart()

    def homepage(self):
        print("Check that we are on homepage and the product list is present containg 3 elements")
        page = HomePage(self.driver)
        page.wait_for_snipcart()
        self.assertTrue(page.is_products_list_present())
